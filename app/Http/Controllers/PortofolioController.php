<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\socmed;
use DB;
use Auth;
use Responsive;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facedes\Redirect;
use Illuminate\Support\Facedes\Validator;
use Illuminate\Support\Facedes\Input;

class PortofolioController extends Controller
{
  public function index()
  {
      $socmeds = socmed::all();
      return view('Portofolio.index', ['socmeds' => $socmeds]);
  }
}
