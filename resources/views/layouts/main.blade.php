<!doctype html>
<html lang="en">
  <head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<meta charset="utf-8">
    <!-- Bootstrap CSS -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="{{asset('img/logo.png')}}" />
    <link rel="stylesheet" href="{{asset('vendor/lightbox/dist/ekko-lightbox.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('css/app.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Big+Shoulders+Text&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab&display=swap" rel="stylesheet">
    <title>Personal Web</title>
  </head>
  <body>
    
  </body>
  <footer id="sticky-footer" class="py-2 bg-dark text-white-50 bottom">
      <!--<div class="row text-center text-lg-center align-items-center">
          <div class="col-md-5 col-sm-5"></div>
          <div class="col-md-2 sosmed-ctm">
            <a href="http://instagram.com/f.attul">
              <img src="{{asset('img/sosmed/li.png')}}" alt="Responsive image" class="img-fluid">
            </a>
          </div>
          <div class="col-md-2 sosmed-ctm">
              <img src="{{asset('img/sosmed/li.png')}}" alt="Responsive image" class="img-fluid">
          </div>
          <div class="col-md-2 sosmed-ctm">
              <img src="../../../public/img/Sosmed/fb.png" alt="Responsive image" class="img-fluid">
          </div>
          <div class="col-md-3"></div>
        </div>-->
      <div class="container text-center">
        <small>Copyright &copy; Your Website Belum</small>
      </div>
    </footer>
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="{{asset('vendor/bootstrap/dist/js/bootstrap.min.js')}}"></script>

    <!-- Plugin JS-->
    <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
    <script src="{{asset('vendor/lightbox/dist/ekko-lightbox.js')}}"></script>

    <!-- Custom JS for Scrolling when Clicked -->
    <script src="{{asset('js/scrolling-nav.js')}}"></script>
    <script src="{{asset('js/lightbox.js')}}"></script>
    </html>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"</script>
