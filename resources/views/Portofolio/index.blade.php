<!-- Navigation -->
@extends('layouts.main')

@section('content')
<!-- Image header -->
<body class="headerbackground" >
  <nav class="navbar-ctm navbar-expand-lg fixed-top">
    <div class="container">
      <ul class="nav justify-content-center">
        <!--<li class="nav-item">
          <a class="nav-link js-scroll-trigger active" href="#biografi">Biografi</a>
        </li>-->
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#about">ArtWork</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#tentang">Tentang Saya</a>
        </li>
      </ul>
    </div>
  </nav>
  <header class="masthead" id="home">
  <div class="container h-100">
    <div class="row h-100 align-items-center">
      <div class="col-md-6">
        <div class="container">
          <div class="container container-ctm">
                <p class="h1-ctm">HI. Saya Luthfi</p>
          </div>
          <div class="paragraf-ctm">
                <p id="perkenalan">Saya adalah seorang mahasiswa teknik Informatika. Mempunyai ketertarikan pada Design, Analisa dan Data.</p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
          <img src="{{asset('img/img1.png')}}" class="img-fluid" alt="Responsive image">
      </div>
    </div>
  </div>
  </div>
</header>
  <br>
  <div class="container" id="about">
    <h3 class="font-weight-light text-center text-lg-left mt-4 mb-0">Saya Bekerja Menggunakan</h3>
    <hr class="mt-2 mb-5">
    <div class="text-center ">
      <img src="{{asset('img/Icon/Office.png')}}" class="icon-img" alt="">
      <img src="{{asset('img/Icon/Ai.png')}}" class="icon-img" alt="">
      <img src="{{asset('img/Icon/Ps.png')}}" class="icon-img" alt="">
      <img src="{{asset('img/Icon/Xd.png')}}" class="icon-img" alt="">
      <img src="{{asset('img/Icon/flutter.png')}}" class="icon-img" alt="">
      <img src="{{asset('img/Icon/Python.png')}}" class="icon-img" alt="">
      <img src="{{asset('img/Icon/r.png')}}" class="icon-img" alt="">
    </div>
  </div>
  <br><br>
  <div class="container">
    <div class="row row-ctm" id="programming">
         <div class="col-md-5">
           <a href="#">
             <img class="img-fluid rounded mb-3 mb-md-0" src="{{asset('img/bg2.png')}}" alt="">
           </a>
         </div>
         <div class="col-md-7 right-col">
           <h3 class="h-ctm">Programming</h3>
           <p class="p-ctm">Beberapa Aplikasi atau Program yang telah saya buat...</p>
           <a class="btn btn-primary" href="https://gitlab.com/esmerelda">Lihat Lebih</a>
         </div>
    </div>
       <div class="row row-ctm-r" id="design">
          <div class="col-md-7 Left-col text-lg-right">
              <h3 class="h-ctm">Design</h3>
              <p class="p-ctm">Beberapa Design yang telah saya buat...</p>
              <a class="btn btn-primary" href="https://www.behance.net/luthfifattulloh">Lihat Lebih</a>
            </div>
          <div class="col-md-5">
            <a href="#">
              <img class="img-fluid rounded mb-3 mb-md-0" src="{{asset('img/bg3.png')}}" alt="">
            </a>
          </div>
     </div>
    <!--<div class="container" id="design">
          <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">Thumbnail Gallery</h1>
          <hr class="mt-2 mb-5">
          <div class="row text-center text-lg-left">
            <div class="col-md-4">
                <img src="../Src/img/Design/1.jpg" alt="Responsive image" class="img-thumbnail">
            </div>
            <div class="col-md-4">
                <img src="../Src/img/Design/2.jpg" alt="Responsive image" class="img-thumbnail">
            </div>
            <div class="col-md-4">
                <img src="../Src/img/Design/3.jpg" alt="Responsive image" class="img-thumbnail">
            </div>
          </div>
          <br>
          <div class="row text-center text-lg-left">
              <div class="col-md-4">
                  <img src="../Src/img/Design/4.png" alt="Responsive image" class="img-thumbnail">
              </div>
              <div class="col-md-4">
                  <img src="../Src/img/Design/5.png" alt="Responsive image" class="img-thumbnail">
              </div>
              <div class="col-md-4">
                  <img src="../Src/img/Design/6.jpg" alt="Responsive image" class="img-thumbnail">
              </div>
            </div>
    </div>-->
    <br>
    <br>
  </div>
  <br>

      <!-- Works 1 -->
      <div class="container" id="tentang">
        <h1 class="text-center h-ctm">Tentang Saya</h1>
        <hr class="mt-2 mb-5">
        <div class="row">
          <div class="col-md-4">
            <img src="{{asset('img/self.png')}}" class="img-fluid rounded" alt="">
          </div>
          <div class="col-md-4">
            <h3>Skill</h3>
            <ul class="list-ctm">
              <li><h4>Design</h4></li>
              <li><h4>Analisa</h4></li>
              <li><h4>Statistika</h4></li>
              <li><h4>Quick Learner</h4></li>
              <li><h4>Creative</h4></li>
            </ul>
            <h3>Find Me On</h3>
            <ul class="list-ctm">
              @foreach($socmeds as $data)
              <li>
                <a href="{{ $data->link }}"><h4>{{ $data->nama }}</h4></a>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
  <br>
  <br>
</body>
